// attendre que l'output buffer recoive une donnee du
// clavier

attente:
    inb $0x64, %al   // AL = status register
    testb $0x01, %al // teste le bit 0 (OUTB) du registre AL
    jz attente 	     // bit 0 = 0 : rien dans l'output buffer

// l'output buffer contient quelque chose : le lire

    inb $0x60, %al   // AL = code de la touche
